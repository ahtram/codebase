using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.XInput;

public class Rhodos : MonoBehaviour, RhodosInputAction.IMainActions {

    //All "button" type. (Some of these are emulated axis directions as buttons)
    public enum InputType {
        Confirm,
        Cancel,
        Option,
        Mod,
        Previous,   //LB
        Next,       //RB
        Select,
        Start,

        //Left axis
        LJL,    //Left
        LJR,    //Right
        LJU,    //Up
        LJD,    //Down

        //Special button
        Backward,   //LT
        Forward,    //RT

        LJ,
        RJ,

        LJLD,   //Left-Down
        LJRD,   //Right-Down
        LJLU,   //Left-Up
        LJRU,   //Right-Up

        //Right axis
        RJL,    //Left
        RJR,    //Right
        RJU,    //Up
        RJD,    //Down

        RJLD,   //Left-Down
        RJRD,   //Right-Down
        RJLU,   //Left-Up
        RJRU,   //Right-Up

        //Cross axis
        CL,     //Left
        CR,     //Right
        CU,     //Up
        CD,     //Down

        CLD,    //Left-Down
        CRD,    //Right-Down
        CLU,    //Left-Up
        CRU,    //Right-Up

        //Combo axis
        CBL,     //Left
        CBR,     //Right
        CBU,     //Up
        CBD,     //Down

        CBLD,    //Left-Down
        CBRD,    //Right-Down
        CBLU,    //Left-Up
        CBRU,    //Right-Up

        //Unique keyboard
        KeyboardEnter,

        None    //This will not appear by logic.
    };

    //Supported Gamepad Type.
    public enum GamepadType {
        XBoxOne,
        PS4,
        None
    };

    //Supported InputDevice Type.
    public enum InputDeviceType {
        Keyboard,
        Gamepad,
        None
    }

    //A Button status struc for tracking the keep state.
    private class ButtonStatus {
        public enum State {
            NotDown,
            Down,
            Keep
        };

        public State state = State.NotDown;
        public bool keepIsActive = false;
        public float accumulateDelay = 0.0f;
    };

    //The InputActionAsset generated script.
    private RhodosInputAction _rohdosInputAction;

    //Start from 0 to N.
    [SerializeField]
    private int _playerNum = 0;

    //Start from 0.
    [SerializeField]
    private InputDeviceType _inputDeviceType = InputDeviceType.Keyboard;

    //The device Id Rhodos has found. (Auto detect)
    //-1 means the specified _playerNum was not found.
    private int _bindedDeviceId = -1;

    //The current binded GamepadType. (None when no GamePad detected)
    private GamepadType _gamepadType = GamepadType.None;
    public UnityEvent<GamepadType> onGamepadTypeChanged;

    //These will be called only when pressed/released.
    public UnityEvent<int, InputType> onButtonDown;
    public UnityEvent<int, InputType> onButtonKeep;
    public UnityEvent<int, InputType> onButtonUp;

    //Ordered by InputType.
    private List<ButtonStatus> _buttonStatus = new List<ButtonStatus>();

    [SerializeField]
    private float _buttonEnterRepeatDelay = 0.2f;
    [SerializeField]
    private float _buttonKeepRepeatInterval = 0.1f;

    //These will be called contineuesly.
    public UnityEvent<int, Vector2> onLeftJoystick;
    public UnityEvent<int, Vector2> onRightJoystick;
    public UnityEvent<int, Vector2> onCrossJoystick;
    //Combo = LeftJoystick and CrossJoystick and VirtualJoystick.
    public UnityEvent<int, Vector2> onComboJoystick;

    //LT and RT are special. They are actually axis and behave differently on XBox and PS controllers.
    public UnityEvent<int, float> onLT;
    public UnityEvent<int, float> onRT;

    //For simulate LT/RT button up/down.
    public const float AXIS_DOWN_VALUE = 0.13f;
    private float _ltValue = 0.0f;
    private float _rtValue = 0.0f;

    //For LJ/RJ/Cross.
    private Vector2 _ljValue = Vector2.zero;
    private Vector2 _rjValue = Vector2.zero;
    private Vector2 _cValue = Vector2.zero;

    //Support for Third-party virtual joystick. (We need a reference to work)
    public VariableJoystick variableJoystick;
    //These will be called only when pressed/released.
    public UnityEvent<int, InputType> onVButtonDown;
    public UnityEvent<int, InputType> onVButtonKeep;
    public UnityEvent<int, InputType> onVButtonUp;
    public UnityEvent<int, Vector2> onVJoystick;

    //The virtual button we're checking in Update()
    [SerializeField]
    private List<RhodosVirtualButton> _virtualButtons = new List<RhodosVirtualButton>();

    //Ordered by KeyConfig.Key enum. (This is for implement onKeyboardKeyKeep onVirtualKeyKeep)
    private List<ButtonStatus> _virtualButtonKeyStatus = new List<ButtonStatus>();

    private Vector2 _vjValue = Vector2.zero;

    [SerializeField]
    public float _virtualButtonEnterRepeatDelay = 0.35f;

    [SerializeField]
    public float _virtualButtonKeepRepeatInterval = 0.1f;

    void Awake() {
        _rohdosInputAction = new RhodosInputAction();

        _buttonStatus.Clear();
        int inputTypeCount = Enum.GetNames(typeof(InputType)).Length;
        for (int i = 0; i < inputTypeCount; i++) {
            _buttonStatus.Add(new ButtonStatus());
        }

        _virtualButtonKeyStatus.Clear();
        for (int i = 0; i < inputTypeCount; i++) {
            _virtualButtonKeyStatus.Add(new ButtonStatus());
        }

        //Huh?
        // _rohdosInputAction.Main.Confirm.actionMap.ApplyBindingOverride
        // foreach (InputBinding inputBinding in _rohdosInputAction.Main.Confirm.bindings) {
        //     Debug.Log(inputBinding.overrideInteractions);
        //     Debug.Log(inputBinding.effectivePath);
        // }

        //Err?
        // foreach (InputDevice inputDevice in InputSystem.devices) {
        //     Debug.Log("[Found InputDevice Type]: " + inputDevice.GetType().ToString());
        // }
    }

    /// <summary>
    /// This will reset all actions to null. Also clear the cache LT/RT value.
    /// </summary>
    public void Reset() {
        onButtonDown.RemoveAllListeners();
        onButtonKeep.RemoveAllListeners();
        onButtonUp.RemoveAllListeners();
        onLeftJoystick.RemoveAllListeners();
        onRightJoystick.RemoveAllListeners();
        onCrossJoystick.RemoveAllListeners();
        onComboJoystick.RemoveAllListeners();
        onVJoystick.RemoveAllListeners();
        _ltValue = 0.0f;
        _rtValue = 0.0f;
        _ljValue = Vector2.zero;
        _rjValue = Vector2.zero;
        _cValue = Vector2.zero;
        _vjValue = Vector2.zero;
    }

    void FixedUpdate() {
        //Keep detect deviceId for _playerNum and _deviceType
        //Let's just face roll this for now...
        detectDeviceBindingForPlayer();
    }

    void detectDeviceBindingForPlayer() {
        List<InputDevice> devices = new List<InputDevice>();
        if (_inputDeviceType == InputDeviceType.Keyboard) {
            devices = InputSystem.devices.Where((inputDevice) => inputDevice.device is Keyboard).ToList();
        } else if (_inputDeviceType == InputDeviceType.Gamepad) {
            devices = InputSystem.devices.Where((inputDevice) => inputDevice.device is Gamepad).ToList();
        }

        if (_playerNum < devices.Count) {
            if (_bindedDeviceId == -1) {
                Debug.Log("[Rhodos] a [" + _inputDeviceType.ToString() + "] was binded for player [" + _playerNum.ToString() + "]");
                _bindedDeviceId = devices[_playerNum].deviceId;
                detectAndNotifyGamepadTypeForDevice(devices[_playerNum]);
            } else if (_bindedDeviceId != devices[_playerNum].deviceId) {
                Debug.Log("[Rhodos] a [" + _inputDeviceType.ToString() + "] was switched binding to player [" + _playerNum.ToString() + "]");
                _bindedDeviceId = devices[_playerNum].deviceId;
                detectAndNotifyGamepadTypeForDevice(devices[_playerNum]);
            }
        } else {
            if (_bindedDeviceId != -1) {
                Debug.Log("[Rhodos] a [" + _inputDeviceType.ToString() + "] has lost binding for player [" + _playerNum.ToString() + "]");
                _bindedDeviceId = -1;
                resetAndNotifyGamepadType();
            }
        }
    }

    void detectAndNotifyGamepadTypeForDevice(InputDevice inputDevice) {
        GamepadType newGamepadType = GamepadType.None;
        if (inputDevice is DualShockGamepad) {
            newGamepadType = GamepadType.PS4;
        } else if (inputDevice is XInputController) {
            newGamepadType = GamepadType.XBoxOne;
        } else if (inputDevice is Gamepad) {
            newGamepadType = GamepadType.XBoxOne;
        } else {
            newGamepadType = GamepadType.None;
        }
        if (newGamepadType != _gamepadType) {
            _gamepadType = newGamepadType;
            Debug.Log("[Rhodos] GamepadType [" + _gamepadType.ToString() + "] for player [" + _playerNum.ToString() + "]");
            onGamepadTypeChanged?.Invoke(_gamepadType);
        }
    }

    void resetAndNotifyGamepadType() {
        if (_gamepadType != GamepadType.None) {
            _gamepadType = GamepadType.None;
            Debug.Log("[Rhodos] GamepadType [" + _gamepadType.ToString() + "] for player [" + _playerNum.ToString() + "]");
            onGamepadTypeChanged?.Invoke(_gamepadType);
        }
    }

    void OnEnable() {
        //!!! Don't change this order !!!
        _rohdosInputAction.Enable();
        //Will this work??? (Yes) (We may make this useful for re-mapping keys)
        // _rohdosInputAction.Main.Confirm.ChangeBinding(0).WithPath("<Keyboard>/g");
        _rohdosInputAction.Main.AddCallbacks(this);
    }

    void OnDisable() {
        //!!! Don't change this order !!!
        _rohdosInputAction.Main.RemoveCallbacks(this);
        _rohdosInputAction.Disable();
    }

    void Update() {
        Vector2 thisLJValue = Vector2.zero;
        Vector2 thisRJValue = Vector2.zero;
        Vector2 thisCValue = Vector2.zero;
        Vector2 thisVJValue = Vector2.zero;

        //Process continues signals...
        //LT/RT
        if (_rohdosInputAction.Main.Backward.activeControl != null && _rohdosInputAction.Main.Backward.activeControl.device.deviceId == _bindedDeviceId) {
            _ltValue = _rohdosInputAction.Main.Backward.ReadValue<float>();
        } else {
            _ltValue = 0.0f;
        }
        if (_rohdosInputAction.Main.Forward.activeControl != null && _rohdosInputAction.Main.Forward.activeControl.device.deviceId == _bindedDeviceId) {
            _rtValue = _rohdosInputAction.Main.Forward.ReadValue<float>();
        } else {
            _rtValue = 0.0f;
        }
        onLT?.Invoke(_playerNum, _ltValue);
        onRT?.Invoke(_playerNum, _rtValue);

        //LJ/RJ
        if (_rohdosInputAction.Main.LJ.activeControl != null && _rohdosInputAction.Main.LJ.activeControl.device.deviceId == _bindedDeviceId) {
            thisLJValue = _rohdosInputAction.Main.LJ.ReadValue<Vector2>();
        } else {
            thisLJValue = Vector2.zero;
        }
        if (_rohdosInputAction.Main.RJ.activeControl != null && _rohdosInputAction.Main.RJ.activeControl.device.deviceId == _bindedDeviceId) {
            thisRJValue = _rohdosInputAction.Main.RJ.ReadValue<Vector2>();
        } else {
            thisRJValue = Vector2.zero;
        }
        onLeftJoystick?.Invoke(_playerNum, thisLJValue);
        onRightJoystick?.Invoke(_playerNum, thisRJValue);

        //Cross
        if (_rohdosInputAction.Main.Cross.activeControl != null && _rohdosInputAction.Main.Cross.activeControl.device.deviceId == _bindedDeviceId) {
            thisCValue = _rohdosInputAction.Main.Cross.ReadValue<Vector2>();
        } else {
            thisCValue = Vector2.zero;
        }
        onCrossJoystick?.Invoke(_playerNum, thisCValue);

        //VJoystick
        if (variableJoystick != null) {
            thisVJValue = variableJoystick.Direction;
            onVJoystick?.Invoke(_playerNum, thisVJValue);
        }

        //Combo
        if (Util.VecEqual(_ljValue, Vector2.zero)) {
            //Return VJ?
            if (variableJoystick == null || Util.VecEqual(variableJoystick.Direction, Vector2.zero)) {
                //Retrun CJ.
                onComboJoystick?.Invoke(_playerNum, _cValue);
            } else {
                //Retrun VJ.
                onComboJoystick?.Invoke(_playerNum, variableJoystick.Direction);
            }
        } else {
            //Return LJ.
            onComboJoystick?.Invoke(_playerNum, _ljValue);
        }

        //----------------------------------------------------------------

        //Simulate detail button up...
        //Combo button down detect.
        bool CBLUp = false;
        bool CBRUp = false;
        bool CBDUp = false;
        bool CBUUp = false;
        bool CBLDUp = false;
        bool CBRDUp = false;
        bool CBLUUp = false;
        bool CBRUUp = false;

        //LJL/LJR/LJD/LJU as buttons.
        if (_ljValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _ljValue.y && _ljValue.y < AXIS_DOWN_VALUE) {
            if (thisLJValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisLJValue.y || thisLJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJR);
                CBRUp = true;
            }
        }
        if (_ljValue.x < -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _ljValue.y && _ljValue.y < AXIS_DOWN_VALUE) {
            if (thisLJValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisLJValue.y || thisLJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJL);
                CBLUp = true;
            }
        }
        if (_ljValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _ljValue.x && _ljValue.x < AXIS_DOWN_VALUE) {
            if (thisLJValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisLJValue.x || thisLJValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJU);
                CBUUp = true;
            }
        }
        if (_ljValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _ljValue.x && _ljValue.x < AXIS_DOWN_VALUE) {
            if (thisLJValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisLJValue.x || thisLJValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJD);
                CBDUp = true;
            }
        }

        //LJLD/LJRD/LJLU/LJRU as buttons.
        if (_ljValue.x <= -AXIS_DOWN_VALUE && _ljValue.y <= -AXIS_DOWN_VALUE) {
            if (thisLJValue.x > -AXIS_DOWN_VALUE || thisLJValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJLD);
                CBLDUp = true;
            }
        }
        if (_ljValue.x >= AXIS_DOWN_VALUE && _ljValue.y <= -AXIS_DOWN_VALUE) {
            if (thisLJValue.x < AXIS_DOWN_VALUE || thisLJValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJRD);
                CBRDUp = true;
            }
        }
        if (_ljValue.x <= -AXIS_DOWN_VALUE && _ljValue.y >= AXIS_DOWN_VALUE) {
            if (thisLJValue.x > -AXIS_DOWN_VALUE || thisLJValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJLU);
                CBLUUp = true;
            }
        }
        if (_ljValue.x >= AXIS_DOWN_VALUE && _ljValue.y >= AXIS_DOWN_VALUE) {
            if (thisLJValue.x < AXIS_DOWN_VALUE || thisLJValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.LJRU);
                CBRUUp = true;
            }
        }

        //RJL/RJR/RJD/RJU as buttons.
        if (_rjValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _rjValue.y && _rjValue.y < AXIS_DOWN_VALUE) {
            if (thisRJValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisRJValue.y || thisRJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJR);
            }
        }
        if (_rjValue.x < -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _rjValue.y && _rjValue.y < AXIS_DOWN_VALUE) {
            if (thisRJValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisRJValue.y || thisRJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJL);
            }
        }
        if (_rjValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _rjValue.x && _rjValue.x < AXIS_DOWN_VALUE) {
            if (thisRJValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisRJValue.x || thisRJValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJU);
            }
        }
        if (_rjValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _rjValue.x && _rjValue.x < AXIS_DOWN_VALUE) {
            if (thisRJValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisRJValue.x || thisRJValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJD);
            }
        }

        //RJLD/RJRD/RJLU/RJRU as buttons.
        if (_rjValue.x <= -AXIS_DOWN_VALUE && _rjValue.y <= -AXIS_DOWN_VALUE) {
            if (thisRJValue.x > -AXIS_DOWN_VALUE || thisRJValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJLD);
            }
        }
        if (_rjValue.x >= AXIS_DOWN_VALUE && _rjValue.y <= -AXIS_DOWN_VALUE) {
            if (thisRJValue.x < AXIS_DOWN_VALUE || thisRJValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJRD);
            }
        }
        if (_rjValue.x <= -AXIS_DOWN_VALUE && _rjValue.y >= AXIS_DOWN_VALUE) {
            if (thisRJValue.x > -AXIS_DOWN_VALUE || thisRJValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJLU);
            }
        }
        if (_rjValue.x >= AXIS_DOWN_VALUE && _rjValue.y >= AXIS_DOWN_VALUE) {
            if (thisRJValue.x < AXIS_DOWN_VALUE || thisRJValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.RJRU);
            }
        }

        //CL/CR/CD/CU as buttons.
        if (_cValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _cValue.y && _cValue.y < AXIS_DOWN_VALUE) {
            if (thisCValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisCValue.y || thisCValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CR);
                CBRUp = true;
            }
        }
        if (_cValue.x < -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _cValue.y && _cValue.y < AXIS_DOWN_VALUE) {
            if (thisCValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisCValue.y || thisCValue.y >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CL);
                CBLUp = true;
            }
        }
        if (_cValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _cValue.x && _cValue.x < AXIS_DOWN_VALUE) {
            if (thisCValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisCValue.x || thisCValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CU);
                CBUUp = true;
            }
        }
        if (_cValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _cValue.x && _cValue.x < AXIS_DOWN_VALUE) {
            if (thisCValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= thisCValue.x || thisCValue.x >= AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CD);
                CBDUp = true;
            }
        }

        //CLD/CRD/CLU/CRU as buttons.
        if (_cValue.x <= -AXIS_DOWN_VALUE && _cValue.y <= -AXIS_DOWN_VALUE) {
            if (thisCValue.x > -AXIS_DOWN_VALUE || thisCValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CLD);
                CBLDUp = true;
            }
        }
        if (_cValue.x >= AXIS_DOWN_VALUE && _cValue.y <= -AXIS_DOWN_VALUE) {
            if (thisCValue.x < AXIS_DOWN_VALUE || thisCValue.y > -AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CRD);
                CBRDUp = true;
            }
        }
        if (_cValue.x <= -AXIS_DOWN_VALUE && _cValue.y >= AXIS_DOWN_VALUE) {
            if (thisCValue.x > -AXIS_DOWN_VALUE || thisCValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CLU);
                CBLUUp = true;
            }
        }
        if (_cValue.x >= AXIS_DOWN_VALUE && _cValue.y >= AXIS_DOWN_VALUE) {
            if (thisCValue.x < AXIS_DOWN_VALUE || thisCValue.y < AXIS_DOWN_VALUE) {
                OnButtonUp(_playerNum, InputType.CRU);
                CBRUUp = true;
            }
        }

        //Virtual joystick as combo button.
        if (variableJoystick != null) {
            //VL/VR/VD/VU as buttons.
            if (_vjValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _vjValue.y && _vjValue.y < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= variableJoystick.Direction.y || variableJoystick.Direction.y >= AXIS_DOWN_VALUE) {
                    CBRUp = true;
                }
            }
            if (_vjValue.x < -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _vjValue.y && _vjValue.y < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= variableJoystick.Direction.y || variableJoystick.Direction.y >= AXIS_DOWN_VALUE) {
                    CBLUp = true;
                }
            }
            if (_vjValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _vjValue.x && _vjValue.x < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= variableJoystick.Direction.x || variableJoystick.Direction.x >= AXIS_DOWN_VALUE) {
                    CBUUp = true;
                }
            }
            if (_vjValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < _vjValue.x && _vjValue.x < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= variableJoystick.Direction.x || variableJoystick.Direction.x >= AXIS_DOWN_VALUE) {
                    CBDUp = true;
                }
            }

            //VLD/VRD/VLU/VRU as buttons.
            if (_vjValue.x <= -AXIS_DOWN_VALUE && _vjValue.y <= -AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x > -AXIS_DOWN_VALUE || variableJoystick.Direction.y > -AXIS_DOWN_VALUE) {
                    CBLDUp = true;
                }
            }
            if (_vjValue.x >= AXIS_DOWN_VALUE && _vjValue.y <= -AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x < AXIS_DOWN_VALUE || variableJoystick.Direction.y > -AXIS_DOWN_VALUE) {
                    CBRDUp = true;
                }
            }
            if (_vjValue.x <= -AXIS_DOWN_VALUE && _vjValue.y >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x > -AXIS_DOWN_VALUE || variableJoystick.Direction.y < AXIS_DOWN_VALUE) {
                    CBLUUp = true;
                }
            }
            if (_vjValue.x >= AXIS_DOWN_VALUE && _vjValue.y >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x < AXIS_DOWN_VALUE || variableJoystick.Direction.y < AXIS_DOWN_VALUE) {
                    CBRUUp = true;
                }
            }
        }

        //CBL/CBR/CBD/CBU
        if (CBRUp)
            OnButtonUp(_playerNum, InputType.CBR);

        if (CBLUp)
            OnButtonUp(_playerNum, InputType.CBL);

        if (CBUUp)
            OnButtonUp(_playerNum, InputType.CBU);

        if (CBDUp)
            OnButtonUp(_playerNum, InputType.CBD);

        //CBLD/CBRD/CBLU/CBRU
        if (CBLDUp)
            OnButtonUp(_playerNum, InputType.CBLD);

        if (CBRDUp)
            OnButtonUp(_playerNum, InputType.CBRD);

        if (CBLUUp)
            OnButtonUp(_playerNum, InputType.CBLU);

        if (CBRUUp)
            OnButtonUp(_playerNum, InputType.CBRU);

        //Check virtual buttons up. (This is wrong! We should not fire OnButtonUp reapeatly!)
        _virtualButtons.ForEach((vButton) => {
            if (vButton.CurrentState != RhodosVirtualButton.State.Down) {
                OnVirtualButtonUp(_playerNum, vButton.inputType);
            }
        });

        //----------------------------------------------------------------

        //Process button keep...
        OnButtonKeep(_playerNum, Time.deltaTime);
        OnVirtualButtonKeep(_playerNum, Time.deltaTime);

        //----------------------------------------------------------------

        //Simulate detail button down...
        //Combo button down detect.
        bool CBLDown = false;
        bool CBRDown = false;
        bool CBDDown = false;
        bool CBUDown = false;
        bool CBLDDown = false;
        bool CBRDDown = false;
        bool CBLUDown = false;
        bool CBRUDown = false;

        //LJL/LJR/LJD/LJU as buttons.
        if (_ljValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _ljValue.y || _ljValue.y >= AXIS_DOWN_VALUE) {
            if (thisLJValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisLJValue.y && thisLJValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJR);
                CBRDown = true;
            }
        }
        if (_ljValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _ljValue.y || _ljValue.y >= AXIS_DOWN_VALUE) {
            if (thisLJValue.x <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisLJValue.y && thisLJValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJL);
                CBLDown = true;
            }
        }
        if (_ljValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _ljValue.x || _ljValue.x >= AXIS_DOWN_VALUE) {
            if (thisLJValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisLJValue.x && thisLJValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJU);
                CBUDown = true;
            }
        }
        if (_ljValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _ljValue.x || _ljValue.x >= AXIS_DOWN_VALUE) {
            if (thisLJValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisLJValue.x && thisLJValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJD);
                CBDDown = true;
            }
        }

        //LJLD/LJRD/LJLU/LJRU as buttons.
        if (_ljValue.x > -AXIS_DOWN_VALUE || _ljValue.y > -AXIS_DOWN_VALUE) {
            if (thisLJValue.x <= -AXIS_DOWN_VALUE && thisLJValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJLD);
                CBLDDown = true;
            }
        }
        if (_ljValue.x < AXIS_DOWN_VALUE || _ljValue.y > -AXIS_DOWN_VALUE) {
            if (thisLJValue.x >= AXIS_DOWN_VALUE && thisLJValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJRD);
                CBRDDown = true;
            }
        }
        if (_ljValue.x > -AXIS_DOWN_VALUE || _ljValue.y < AXIS_DOWN_VALUE) {
            if (thisLJValue.x <= -AXIS_DOWN_VALUE && thisLJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJLU);
                CBLUDown = true;
            }
        }
        if (_ljValue.x < AXIS_DOWN_VALUE || _ljValue.y < AXIS_DOWN_VALUE) {
            if (thisLJValue.x >= AXIS_DOWN_VALUE && thisLJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.LJRU);
                CBRUDown = true;
            }
        }

        //RJL/RJR/RJD/RJU as buttons.
        if (_rjValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _rjValue.y || _rjValue.y >= AXIS_DOWN_VALUE) {
            if (thisRJValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisRJValue.y && thisRJValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJR);
            }
        }
        if (_rjValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _rjValue.y || _rjValue.y >= AXIS_DOWN_VALUE) {
            if (thisRJValue.x <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisRJValue.y && thisRJValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJL);
            }
        }
        if (_rjValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _rjValue.x || _rjValue.x >= AXIS_DOWN_VALUE) {
            if (thisRJValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisRJValue.x && thisRJValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJU);
            }
        }
        if (_rjValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _rjValue.x || _rjValue.x >= AXIS_DOWN_VALUE) {
            if (thisRJValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisRJValue.x && thisRJValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJD);
            }
        }

        //RJLD/RJRD/RJLU/RJRU as buttons.
        if (_rjValue.x > -AXIS_DOWN_VALUE || _rjValue.y > -AXIS_DOWN_VALUE) {
            if (thisRJValue.x <= -AXIS_DOWN_VALUE && thisRJValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJLD);
            }
        }
        if (_rjValue.x < AXIS_DOWN_VALUE || _rjValue.y > -AXIS_DOWN_VALUE) {
            if (thisRJValue.x >= AXIS_DOWN_VALUE && thisRJValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJRD);
            }
        }
        if (_rjValue.x > -AXIS_DOWN_VALUE || _rjValue.y < AXIS_DOWN_VALUE) {
            if (thisRJValue.x <= -AXIS_DOWN_VALUE && thisRJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJLU);
            }
        }
        if (_rjValue.x < AXIS_DOWN_VALUE || _rjValue.y < AXIS_DOWN_VALUE) {
            if (thisRJValue.x >= AXIS_DOWN_VALUE && thisRJValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.RJRU);
            }
        }

        //CL/CR/CD/CU as buttons.
        if (_cValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _cValue.y || _cValue.y >= AXIS_DOWN_VALUE) {
            if (thisCValue.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisCValue.y && thisCValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CR);
                CBRDown = true;
            }
        }
        if (_cValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _cValue.y || _cValue.y >= AXIS_DOWN_VALUE) {
            if (thisCValue.x <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisCValue.y && thisCValue.y < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CL);
                CBLDown = true;
            }
        }
        if (_cValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _cValue.x || _cValue.x >= AXIS_DOWN_VALUE) {
            if (thisCValue.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisCValue.x && thisCValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CU);
                CBUDown = true;
            }
        }
        if (_cValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _cValue.x || _cValue.x >= AXIS_DOWN_VALUE) {
            if (thisCValue.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < thisCValue.x && thisCValue.x < AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CD);
                CBDDown = true;
            }
        }

        //CLD/CRD/CLU/CRU as buttons.
        if (_cValue.x > -AXIS_DOWN_VALUE || _cValue.y > -AXIS_DOWN_VALUE) {
            if (thisCValue.x <= -AXIS_DOWN_VALUE && thisCValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CLD);
                CBLDDown = true;
            }
        }
        if (_cValue.x < AXIS_DOWN_VALUE || _cValue.y > -AXIS_DOWN_VALUE) {
            if (thisCValue.x >= AXIS_DOWN_VALUE && thisCValue.y <= -AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CRD);
                CBRDDown = true;
            }
        }
        if (_cValue.x > -AXIS_DOWN_VALUE || _cValue.y < AXIS_DOWN_VALUE) {
            if (thisCValue.x <= -AXIS_DOWN_VALUE && thisCValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CLU);
                CBLUDown = true;
            }
        }
        if (_cValue.x < AXIS_DOWN_VALUE || _cValue.y < AXIS_DOWN_VALUE) {
            if (thisCValue.x >= AXIS_DOWN_VALUE && thisCValue.y >= AXIS_DOWN_VALUE) {
                OnButtonDown(_playerNum, InputType.CRU);
                CBRUDown = true;
            }
        }

        //Virtual joystick as combo button.
        if (variableJoystick != null) {
            //VL/VR/VD/VU as buttons.
            if (_vjValue.x < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _vjValue.y || _vjValue.y >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < variableJoystick.Direction.y && variableJoystick.Direction.y < AXIS_DOWN_VALUE) {
                    CBRDown = true;
                }
            }
            if (_vjValue.x > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _vjValue.y || _vjValue.y >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < variableJoystick.Direction.y && variableJoystick.Direction.y < AXIS_DOWN_VALUE) {
                    CBLDown = true;
                }
            }
            if (_vjValue.y < AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _vjValue.x || _vjValue.x >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.y >= AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < variableJoystick.Direction.x && variableJoystick.Direction.x < AXIS_DOWN_VALUE) {
                    CBUDown = true;
                }
            }
            if (_vjValue.y > -AXIS_DOWN_VALUE || -AXIS_DOWN_VALUE >= _vjValue.x || _vjValue.x >= AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.y <= -AXIS_DOWN_VALUE && -AXIS_DOWN_VALUE < variableJoystick.Direction.x && variableJoystick.Direction.x < AXIS_DOWN_VALUE) {
                    CBDDown = true;
                }
            }

            //VLD/VRD/VLU/VRU as buttons.
            if (_vjValue.x > -AXIS_DOWN_VALUE || _vjValue.y > -AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x <= -AXIS_DOWN_VALUE && variableJoystick.Direction.y <= -AXIS_DOWN_VALUE) {
                    CBLDDown = true;
                }
            }
            if (_vjValue.x < AXIS_DOWN_VALUE || _vjValue.y > -AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x >= AXIS_DOWN_VALUE && variableJoystick.Direction.y <= -AXIS_DOWN_VALUE) {
                    CBRDDown = true;
                }
            }
            if (_vjValue.x > -AXIS_DOWN_VALUE || _vjValue.y < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x <= -AXIS_DOWN_VALUE && variableJoystick.Direction.y >= AXIS_DOWN_VALUE) {
                    CBLUDown = true;
                }
            }
            if (_vjValue.x < AXIS_DOWN_VALUE || _vjValue.y < AXIS_DOWN_VALUE) {
                if (variableJoystick.Direction.x >= AXIS_DOWN_VALUE && variableJoystick.Direction.y >= AXIS_DOWN_VALUE) {
                    CBRUDown = true;
                }
            }
        }

        //Combo button down detect.

        //CBL/CBR/CBD/CBU
        if (CBRDown)
            OnButtonDown(_playerNum, InputType.CBR);

        if (CBLDown)
            OnButtonDown(_playerNum, InputType.CBL);

        if (CBUDown)
            OnButtonDown(_playerNum, InputType.CBU);

        if (CBDDown)
            OnButtonDown(_playerNum, InputType.CBD);

        //CBLD/CBRD/CBLU/CBRU
        if (CBLDDown)
            OnButtonDown(_playerNum, InputType.CBLD);

        if (CBRDDown)
            OnButtonDown(_playerNum, InputType.CBRD);

        if (CBLUDown)
            OnButtonDown(_playerNum, InputType.CBLU);

        if (CBRUDown)
            OnButtonDown(_playerNum, InputType.CBRU);

        //Check virtual buttons down.
        _virtualButtons.ForEach((vButton) => {
            if (vButton.CurrentState == RhodosVirtualButton.State.Down) {
                OnVirtualButtonDown(_playerNum, vButton.inputType);
            }
        });

        //!! Do these last !!! Cache axis value for each frame for emulate axis direction as buttons!
        _ljValue = thisLJValue;
        _rjValue = thisRJValue;
        _cValue = thisCValue;
        _vjValue = thisVJValue;
    }

    private void OnButtonDown(int playerNum, InputType inputType) {
        if (_buttonStatus[(int)inputType].state == ButtonStatus.State.NotDown) {
            _buttonStatus[(int)inputType].state = ButtonStatus.State.Down;
            // Debug.Log("[Rhodos][OnButtonDown][" + playerNum + "][" + inputType.ToString() + "]");
            onButtonDown?.Invoke(playerNum, inputType);
        }
    }

    private void OnButtonUp(int playerNum, InputType inputType) {
        if (_buttonStatus[(int)inputType].state != ButtonStatus.State.NotDown) {
            _buttonStatus[(int)inputType].state = ButtonStatus.State.NotDown;
            _buttonStatus[(int)inputType].accumulateDelay = 0.0f;
            // Debug.Log("[Rhodos][OnButtonUp][" + playerNum + "][" + inputType.ToString() + "]");
            onButtonUp?.Invoke(playerNum, inputType);
        }
    }

    private void OnButtonKeep(int playerNum, float deltaTime) {
        for (int i = 0; i < _buttonStatus.Count; i++) {
            if (_buttonStatus[i].state == ButtonStatus.State.Down) {
                _buttonStatus[i].accumulateDelay += deltaTime;
                if (_buttonStatus[i].accumulateDelay >= _buttonEnterRepeatDelay) {
                    _buttonStatus[i].state = ButtonStatus.State.Keep;
                    _buttonStatus[i].accumulateDelay -= _buttonEnterRepeatDelay;
                    // Debug.Log("[Rhodos][OnButtonKeep][" + playerNum + "][" + Enum.GetNames(typeof(InputType))[i] + "]");
                    onButtonKeep?.Invoke(playerNum, (InputType)i);
                }
            } else if (_buttonStatus[i].state == ButtonStatus.State.Keep) {
                _buttonStatus[i].accumulateDelay += deltaTime;
                if (_buttonStatus[i].accumulateDelay >= _buttonKeepRepeatInterval) {
                    _buttonStatus[i].accumulateDelay -= _buttonKeepRepeatInterval;
                    // Debug.Log("[Rhodos][OnButtonKeep][" + playerNum + "][" + i.ToString() + "]");
                    onButtonKeep?.Invoke(playerNum, (InputType)i);
                }
            }
        }
    }

    //========= Virtual Buttons ===========

    private void OnVirtualButtonDown(int _playerNum, InputType inputType) {
        if (_virtualButtonKeyStatus[(int)inputType].state == ButtonStatus.State.NotDown) {
            _virtualButtonKeyStatus[(int)inputType].state = ButtonStatus.State.Down;
            onVButtonDown?.Invoke(_playerNum, inputType);
        }
    }

    private void OnVirtualButtonUp(int _playerNum, InputType inputType) {
        if (_virtualButtonKeyStatus[(int)inputType].state != ButtonStatus.State.NotDown) {
            _virtualButtonKeyStatus[(int)inputType].state = ButtonStatus.State.NotDown;
            _virtualButtonKeyStatus[(int)inputType].accumulateDelay = 0.0f;
            onVButtonUp?.Invoke(_playerNum, inputType);
        }
    }

    private void OnVirtualButtonKeep(int playerNum, float deltaTime) {
        for (int i = 0; i < _virtualButtonKeyStatus.Count; i++) {
            if (_virtualButtonKeyStatus[i].state == ButtonStatus.State.Down) {
                _virtualButtonKeyStatus[i].accumulateDelay += deltaTime;
                if (_virtualButtonKeyStatus[i].accumulateDelay >= _buttonEnterRepeatDelay) {
                    _virtualButtonKeyStatus[i].state = ButtonStatus.State.Keep;
                    _virtualButtonKeyStatus[i].accumulateDelay -= _buttonEnterRepeatDelay;
                    onVButtonKeep?.Invoke(playerNum, (InputType)i);
                }
            } else if (_virtualButtonKeyStatus[i].state == ButtonStatus.State.Keep) {
                _virtualButtonKeyStatus[i].accumulateDelay += deltaTime;
                if (_virtualButtonKeyStatus[i].accumulateDelay >= _buttonKeepRepeatInterval) {
                    _virtualButtonKeyStatus[i].accumulateDelay -= _buttonKeepRepeatInterval;
                    onVButtonKeep?.Invoke(playerNum, (InputType)i);
                }
            }
        }
    }

    //-- Callbacks for RhodosInputAction.

    public void OnConfirm(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Confirm);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Confirm);
                    break;
            }
        }
    }

    public void OnCancel(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Cancel);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Cancel);
                    break;
            }
        }
    }

    public void OnOption(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Option);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Option);
                    break;
            }
        }
    }

    public void OnMod(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Mod);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Mod);
                    break;
            }
        }
    }

    public void OnPrevious(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Previous);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Previous);
                    break;
            }
        }
    }

    public void OnNext(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Next);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Next);
                    break;
            }
        }
    }

    public void OnBackward(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Backward);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Backward);
                    break;
            }
        }
    }

    public void OnForward(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Forward);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Forward);
                    break;
            }
        }
    }

    public void OnSelect(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Select);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Select);
                    break;
            }
        }
    }

    public void OnStart(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.Start);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.Start);
                    break;
            }
        }
    }

    public void OnCross(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            //We don't need this.
        }
    }

    public void OnLJ(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            //We don't need this.
        }
    }

    public void OnLJB(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.LJ);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.LJ);
                    break;
            }
        }
    }

    public void OnRJ(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            //We don't need this.
        }
    }

    public void OnRJB(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.RJ);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.RJ);
                    break;
            }
        }
    }

    //These are copies from DefaultInputActions.

    public void OnPoint(InputAction.CallbackContext context) {

    }

    public void OnClick(InputAction.CallbackContext context) {

    }

    public void OnScrollWheel(InputAction.CallbackContext context) {

    }

    public void OnMiddleClick(InputAction.CallbackContext context) {

    }

    public void OnRightClick(InputAction.CallbackContext context) {

    }

    public void OnTrackedDevicePosition(InputAction.CallbackContext context) {

    }

    public void OnTrackedDeviceOrientation(InputAction.CallbackContext context) {

    }

    public void OnEnter(InputAction.CallbackContext context) {
        if (context.control.device.deviceId == _bindedDeviceId) {
            switch (context.phase) {
                case InputActionPhase.Started:
                    OnButtonDown(_playerNum, InputType.KeyboardEnter);
                    break;
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    break;
                case InputActionPhase.Performed:
                    break;
                case InputActionPhase.Canceled:
                    OnButtonUp(_playerNum, InputType.KeyboardEnter);
                    break;
            }
        }
    }

}
