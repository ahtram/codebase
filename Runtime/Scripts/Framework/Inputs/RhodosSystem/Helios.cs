
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// A component for manage one Gamepad Rhodos and one Keyboard Rhodos. Provide interface and events for keyboard and controllers.
/// </summary>
public class Helios : MonoBehaviour {

    public enum DeviceType {
        Keyboard,
        Gamepad,
        VirtualController
    };

    //These will be called only when pressed/released.
    //[Keyboard]
    public UnityEvent<Rhodos.InputType> onKeyboardDown = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onKeyboardKeep = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onKeyboardUp = new UnityEvent<Rhodos.InputType>();
    //These will be called contineuesly.
    public UnityEvent<Vector2> onKeyboardAxis = new UnityEvent<Vector2>();

    //[Gamepad]
    public UnityEvent<Rhodos.InputType> onGamepadDown = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onGamepadKeep = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onGamepadUp = new UnityEvent<Rhodos.InputType>();

    //[Gamepad]
    public UnityEvent<Rhodos.InputType> onVirtualControllerDown = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onVirtualControllerKeep = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onVirtualControllerUp = new UnityEvent<Rhodos.InputType>();

    //These will be called contineuesly.
    //[Gamepad]
    public UnityEvent<Vector2> onGamepadLeftAxis = new UnityEvent<Vector2>();
    public UnityEvent<Vector2> onGamepadRightAxis = new UnityEvent<Vector2>();
    public UnityEvent<Vector2> onGamepadCrossAxis = new UnityEvent<Vector2>();
    //Combo = LeftJoystick and CrossJoystick.
    public UnityEvent<Vector2> onGamepadComboAxis = new UnityEvent<Vector2>();

    //[Gamepad]
    public UnityEvent<Rhodos.GamepadType> onGamepadTypeChanged;

    //Virtual = On screen virtual joystick.
    public UnityEvent<Vector2> onVJoystickAxis = new UnityEvent<Vector2>();

    //If not normalized:
    //The LT/RT value for XBox is 0~1
    //The LT/RT value for PS4 is -1~1
    public UnityEvent<float> onGamepadLTAxis = new UnityEvent<float>();
    public UnityEvent<float> onGamepadRTAxis = new UnityEvent<float>();

    //These will be called both when a Keyboard key or a GamePad key is pressed.
    public UnityEvent<Rhodos.InputType> onInputDown = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onInputKeep = new UnityEvent<Rhodos.InputType>();
    public UnityEvent<Rhodos.InputType> onInputUp = new UnityEvent<Rhodos.InputType>();

    //This will be called when focus key device changed.
    public UnityEvent<DeviceType> onFocusDeviceChanged = new UnityEvent<DeviceType>();

    //This will cache the most recent used key device type.
    private DeviceType focusingDeviceType = DeviceType.Keyboard;

    //Current detected GamePadType.
    private Rhodos.GamepadType currentGamepadType = Rhodos.GamepadType.None;

    //[Gamepad]
    public void OnGamepadButtonDown(int _, Rhodos.InputType inputType) {
        onGamepadDown?.Invoke(inputType);
        onInputDown?.Invoke(inputType);
        CheckSwitchFocusDevice(DeviceType.Gamepad);
    }

    //[Gamepad]
    public void OnGamepadButtonKeep(int _, Rhodos.InputType inputType) {
        onGamepadKeep?.Invoke(inputType);
        onInputKeep?.Invoke(inputType);
    }

    //[Gamepad]
    public void OnGamepadButtonUp(int _, Rhodos.InputType inputType) {
        onGamepadUp?.Invoke(inputType);
        onInputUp?.Invoke(inputType);
    }

    //[Gamepad]
    public void OnGamepadLeftJoystick(int _, Vector2 axisValue) {
        onGamepadLeftAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]
    public void OnGamepadRightJoystick(int _, Vector2 axisValue) {
        onGamepadRightAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]
    public void OnGamepadCrossJoystick(int _, Vector2 axisValue) {
        onGamepadCrossAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]
    public void OnGamepadComboJoystick(int _, Vector2 axisValue) {
        onGamepadComboAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]: LT
    //If not normalized:
    //The LT/RT value for XBox is 0~1
    //The LT/RT value for PS4 is -1~1
    public void OnGamepadLT(int _, float value) {
        onGamepadLTAxis?.Invoke(value);

        if (value >= Rhodos.AXIS_DOWN_VALUE) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]: RT
    //If not normalized:
    //The LT/RT value for XBox is 0~1
    //The LT/RT value for PS4 is -1~1
    public void OnGamepadRT(int _, float value) {
        onGamepadRTAxis?.Invoke(value);

        if (value >= Rhodos.AXIS_DOWN_VALUE) {
            CheckSwitchFocusDevice(DeviceType.Gamepad);
        }
    }

    //[Gamepad]: Type changed
    public void OnGamepadTypeChanged(Rhodos.GamepadType gamepadType) {
        currentGamepadType = gamepadType;
        onGamepadTypeChanged?.Invoke(currentGamepadType);
    }

    //[VirtualController]
    public void OnVirtualControllerButtonDown(int _, Rhodos.InputType inputType) {
        onVirtualControllerDown?.Invoke(inputType);
        onInputDown?.Invoke(inputType);
        CheckSwitchFocusDevice(DeviceType.VirtualController);
    }

    //[VirtualController]
    public void OnVirtualControllerButtonKeep(int _, Rhodos.InputType inputType) {
        onVirtualControllerKeep?.Invoke(inputType);
        onInputKeep?.Invoke(inputType);
    }

    //[VirtualController]
    public void OnVirtualControllerButtonUp(int _, Rhodos.InputType inputType) {
        onVirtualControllerUp?.Invoke(inputType);
        onInputUp?.Invoke(inputType);
    }

    //[VirtualController]
    public void OnVirtualControllerJoystick(int _, Vector2 axisValue) {
        onVJoystickAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.VirtualController);
        }
    }

    //--

    //[Keyboard]
    public void OnKeyboardAxis(int _, Vector2 axisValue) {
        onKeyboardAxis?.Invoke(axisValue);

        if (axisValue != Vector2.zero) {
            CheckSwitchFocusDevice(DeviceType.Keyboard);
        }
    }

    //[Keyboard]
    public void OnKeyboardButtonDown(int _, Rhodos.InputType inputType) {
        onKeyboardDown?.Invoke(inputType);
        onInputDown?.Invoke(inputType);
        CheckSwitchFocusDevice(DeviceType.Keyboard);
    }

    //[Keyboard]
    public void OnKeyboardButtonKeep(int _, Rhodos.InputType inputType) {
        onKeyboardKeep?.Invoke(inputType);
        onInputKeep?.Invoke(inputType);
    }

    //[Keyboard]
    public void OnKeyboardButtonUp(int _, Rhodos.InputType inputType) {
        onKeyboardUp?.Invoke(inputType);
        onInputUp?.Invoke(inputType);
    }

    //==

    //[Focusing Device]
    private void CheckSwitchFocusDevice(DeviceType deviceType) {
        if (deviceType != focusingDeviceType) {
            focusingDeviceType = deviceType;
            Debug.Log("[Helios]: Focus Device Changed [" + focusingDeviceType.ToString() + "]");
            onFocusDeviceChanged?.Invoke(focusingDeviceType);
        }
    }

    /// <summary>
    /// Get the focusing device type.
    /// </summary> 
    public DeviceType FocusingDeviceType {
        get {
            return focusingDeviceType;
        }
    }

    /// <summary>
    /// Get the current GamepadType.
    /// </summary> <summary>
    public Rhodos.GamepadType CurrentGamepadType {
        get {
            return currentGamepadType;
        }
    }

}
