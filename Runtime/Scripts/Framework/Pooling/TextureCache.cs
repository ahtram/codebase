﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This class will cache any requested Texture.
//You can clear the cache before any scene switching to prevent it from bloating.
static public class TextureCache {

    //Main cache body.
    static private Dictionary<string, Texture2D> m_textureCache2D = new Dictionary<string, Texture2D>();
    static private Dictionary<string, Texture> m_textureCache = new Dictionary<string, Texture>();

    //Try get the sprite from cache. If it's not exist in the cache than load and cache and return it.
    static public Texture2D Get2D(string path) {

        //Return the cache if it's exist.
        Texture2D returnTexture2D;
        if (m_textureCache2D.TryGetValue(path, out returnTexture2D)) {
            return returnTexture2D;
        }

        //Cache not exist. Load the sprite and cache it.
        returnTexture2D = Resources.Load<Texture2D>(path);
        if (returnTexture2D != null) {
            m_textureCache2D.Add(path, returnTexture2D);
        }

        //Return the loaded sprite whatever if it's null. (It will possible be null if this sprite is not exist!)
        return returnTexture2D;
    }

    static public Texture Get(string path) {

        //Return the cache if it's exist.
        Texture returnTexture;
        if (m_textureCache.TryGetValue(path, out returnTexture)) {
            return returnTexture;
        }

        //Cache not exist. Load the sprite and cache it.
        returnTexture = Resources.Load<Texture>(path);
        if (returnTexture != null) {
            m_textureCache.Add(path, returnTexture);
        }

        //Return the loaded sprite whatever if it's null. (It will possible be null if this sprite is not exist!)
        return returnTexture;
    }

    static public IEnumerator Get2DAsync(string path, System.Action<Texture2D> onFinish) {
        Texture2D returnTexture2D;
        if (m_textureCache2D.TryGetValue(path, out returnTexture2D)) {
            if (onFinish != null) {
                onFinish(returnTexture2D);
            }
        } else {
            ResourceRequest resourceRequest = Resources.LoadAsync(path, typeof(Texture2D));
            while (!resourceRequest.isDone) {
                yield return null;
            }
            returnTexture2D = (Texture2D)resourceRequest.asset;

            if (returnTexture2D != null) {
                if (!m_textureCache2D.ContainsKey(path)) {
                    m_textureCache2D.Add(path, returnTexture2D);
                }
                if (onFinish != null) {
                    onFinish(returnTexture2D);
                }
            }

            if (onFinish != null) {
                onFinish(returnTexture2D);
            }
        }
    }

    static public IEnumerator GetAsync(string path, System.Action<Texture> onFinish) {
        Texture returnTexture;
        if (m_textureCache.TryGetValue(path, out returnTexture)) {
            if (onFinish != null) {
                onFinish(returnTexture);
            }
        } else {
            ResourceRequest resourceRequest = Resources.LoadAsync(path, typeof(Texture));
            while (!resourceRequest.isDone) {
                yield return null;
            }
            returnTexture = (Texture)resourceRequest.asset;

            if (returnTexture != null) {
                if (!m_textureCache.ContainsKey(path)) {
                    m_textureCache.Add(path, returnTexture);
                }
                if (onFinish != null) {
                    onFinish(returnTexture);
                }
            }

            if (onFinish != null) {
                onFinish(returnTexture);
            }
        }
    }

    //Clear the cache.
    static public void Clear() {
        m_textureCache2D.Clear();
        m_textureCache.Clear();
    }

}
