﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;
using System.Threading.Tasks;
using System.Diagnostics;

#if UNITY_EDITOR
using UnityEditor;
#endif

//This component use a Unity MAGIC someone found here: 
//https://www.reddit.com/r/Unity3D/comments/17eikh0/i_found_a_way_to_go_to_the_right_line_in_your/
//使用UGUI系統製作的Console類別，可用在執行階段於銀幕上顯示字串。請愛用Project內的Console Prefab來做客製化。
public class ScreenLogger : MonoBehaviour {

    //Console Text主體
    public TextMeshProUGUI consoleText;
    public CanvasGroup canvasGroup;
    public ScrollRect scrollRect;

    //同時也顯示在 Unity console 視窗內。
    public bool unityConsoleLog = true;

    //記錄睛顯示的所有字串。做成Queue型式以便先進先出。
    static private Queue<string> m_consoleStrings = new Queue<string>();

    //最多顯示幾筆字串?
    public int maxConsoleLine = 30;

    static private ScreenLogger instance = null;

    void Awake() {
        instance = this;
    }

    //清除畫面上的所有字串。
    static public void CLR() {
        if (instance != null) {
            //清除所有顯示中字串。包含Queue與顯示用Component。
            m_consoleStrings.Clear();
            instance.consoleText.text = "";
        }
    }

    //顯示一行白色字串。
    static public async void Log(string strIn, Color? color = null) {
        if (instance != null) {
            //限叔摦示筆數。
            if (m_consoleStrings.Count >= instance.maxConsoleLine) {
                m_consoleStrings.Dequeue();
            }

            Color consoleColor = color ?? Color.white;

            string consoleStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(consoleColor));
            m_consoleStrings.Enqueue(consoleStrIn);

            if (instance.unityConsoleLog) {
                if (color == null) {
                    UnityEngine.Debug.Log(strIn);
                } else {
                    UnityEngine.Debug.Log(consoleStrIn);
                }
            }

            //輸出字串於Component上。
            RefreshConsoleText();

            await Task.Delay(1);

            if (instance.scrollRect != null) {
                instance.scrollRect.normalizedPosition = new Vector2(0.0f, 1.0f);
            }
        }
    }

    //顯示一行綠色字串。
    static public async void LogGood(string strIn) {
        if (instance != null) {
            if (m_consoleStrings.Count >= instance.maxConsoleLine) {
                m_consoleStrings.Dequeue();
            }

            Color consoleColor = ColorPlus.Chartreuse;

            string consoleStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(consoleColor));
            m_consoleStrings.Enqueue(consoleStrIn);

            if (instance.unityConsoleLog) {
                Color editorColor = ColorPlus.Chartreuse;
#if UNITY_EDITOR
                if (EditorGUIUtility.isProSkin) {
                    editorColor = ColorPlus.Chartreuse;
                } else {
                    editorColor = ColorPlus.DarkGreen;
                }
#else
                UnityEngine.Debug.Log(strIn);
#endif
                string editorStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(editorColor));
                UnityEngine.Debug.Log(editorStrIn);
            }

            RefreshConsoleText();

            await Task.Delay(1);

            if (instance.scrollRect != null) {
                instance.scrollRect.normalizedPosition = new Vector2(0.0f, 0.0f);
            }
        }
    }

    //顯示一行黃色字串。
    static public async void LogWarning(string strIn) {
        if (instance != null) {
            if (m_consoleStrings.Count >= instance.maxConsoleLine) {
                m_consoleStrings.Dequeue();
            }

            Color consoleColor = ColorPlus.Gold;

            string consoleStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(consoleColor));
            m_consoleStrings.Enqueue(consoleStrIn);

            if (instance.unityConsoleLog) {
                Color editorColor = ColorPlus.Gold;
#if UNITY_EDITOR
                if (EditorGUIUtility.isProSkin) {
                    editorColor = ColorPlus.Gold;
                } else {
                    editorColor = ColorPlus.SaddleBrown;
                }
#else
                UnityEngine.Debug.LogWarning(strIn);
#endif
                string editorStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(editorColor));
                UnityEngine.Debug.LogWarning(editorStrIn);
            }

            RefreshConsoleText();

            await Task.Delay(1);

            if (instance.scrollRect != null) {
                instance.scrollRect.normalizedPosition = new Vector2(0.0f, 0.0f);
            }
        }
    }

    //顯示一行錯誤字串。
    static public async void LogError(string strIn) {
        if (instance != null) {
            if (m_consoleStrings.Count >= instance.maxConsoleLine) {
                m_consoleStrings.Dequeue();
            }

            Color consoleColor = ColorPlus.HotPink;

            string consoleStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(consoleColor));
            m_consoleStrings.Enqueue(consoleStrIn);

            if (instance.unityConsoleLog) {
                Color editorColor = ColorPlus.HotPink;
#if UNITY_EDITOR
                if (EditorGUIUtility.isProSkin) {
                    editorColor = ColorPlus.HotPink;
                } else {
                    editorColor = ColorPlus.DarkRed;
                }
#else
                UnityEngine.Debug.LogError(strIn);
#endif
                string editorStrIn = Util.MarkupWithColorTag(strIn, ColorUtility.ToHtmlStringRGB(editorColor));
                UnityEngine.Debug.LogError(editorStrIn);
            }

            RefreshConsoleText();

            await Task.Delay(1);

            if (instance.scrollRect != null) {
                instance.scrollRect.normalizedPosition = new Vector2(0.0f, 0.0f);
            }
        }
    }

    //換行。
    static public void NewLine() {
        if (instance != null) {
            Log("");
        }
    }

    //當前Console是否被顯示?
    static public bool IsShowing() {
        if (instance != null) {
            return (instance.canvasGroup.alpha != 0);
        } else {
            return false;
        }
    }

    //顯示Console?
    static public void Show() {
        if (instance != null) {
            instance.canvasGroup.alpha = 1;
            instance.canvasGroup.interactable = true;
            instance.canvasGroup.blocksRaycasts = true;
        }
    }

    //隱藏Console?
    static public void Hide() {
        if (instance != null) {
            instance.canvasGroup.alpha = 0;
            instance.canvasGroup.interactable = false;
            instance.canvasGroup.blocksRaycasts = false;
        }
    }

    //切換隱藏顯示
    static public void SwitchVisible() {
        if (instance != null) {
            if (instance.canvasGroup.alpha == 0) {
                instance.canvasGroup.alpha = 1;
                instance.canvasGroup.interactable = true;
                instance.canvasGroup.blocksRaycasts = true;
            } else {
                instance.canvasGroup.alpha = 0;
                instance.canvasGroup.interactable = false;
                instance.canvasGroup.blocksRaycasts = false;
            }
        }
    }

    //更新當前Queue內的所有字串至Component上。
    static private void RefreshConsoleText() {
        if (instance != null) {
            string outString = "";
            foreach (string str in m_consoleStrings) {
                outString += ":";
                outString += str;
                outString += System.Environment.NewLine;
            }

            instance.consoleText.text = outString;
        }
    }

    //--

    //This will search and find the link target file StackFrame we want.
    static private StackFrame GetThirdStackFrameWithFileName() {
        int foundStackFrameWithFileName = 0;
        for (int i = 0; i < 33; ++i) {
            StackFrame stackFrame = new(i, true);
            if (stackFrame != null && !string.IsNullOrEmpty(stackFrame.GetFileName())) {
                foundStackFrameWithFileName++;
                if (foundStackFrameWithFileName == 3) {
                    return stackFrame;
                }
            }
        }
        //Not found.
        return null;
    }

}
