﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

//Thanks for: https://stackoverflow.com/questions/26678181/enum-parse-vs-switch-performance
//This help to speed up enum to string or string to enum performance when you need to convert them many many times. 
public class EnumUtil<TTarget> {
    private readonly Dictionary<string, TTarget> _stringToEnumDict;
    private readonly Dictionary<int, string> _enumToStringDict;

    public EnumUtil() {
        //We scan the enum type and prepare for cached dictionaries.
        string[] names = Enum.GetNames(typeof(TTarget));
        _stringToEnumDict = new Dictionary<string, TTarget>();
        _enumToStringDict = new Dictionary<int, string>();
        for (int i = 0; i < names.Length; i++) {
            TTarget enumValue = (TTarget)Enum.Parse(typeof(TTarget), names[i]);
            _stringToEnumDict.Add(names[i], enumValue);
            int intValue = (int)Convert.ChangeType(enumValue, typeof(int));
            if (!_enumToStringDict.ContainsKey(intValue)) {
                _enumToStringDict.Add(intValue, names[i]);
            } else {
                //This warnning will pop-up for Unity Input.KeyCode because they did tricky stuff in their shit.
                // Debug.LogWarning("Oops! Key exist:(" + intValue + "): " + (TTarget)Enum.Parse(typeof(TTarget), names[i], false));
            }
        }
    }

    //String to enum.
    public TTarget ToEnum(string value) {
        return _stringToEnumDict[value];
    }

    //Enum to string.
    public string ToString(TTarget target) {
        int intValue = (int)Convert.ChangeType(target, typeof(int));
        if (_enumToStringDict.ContainsKey(intValue)) {
            return _enumToStringDict[intValue];
        }
        return "";
    }
}