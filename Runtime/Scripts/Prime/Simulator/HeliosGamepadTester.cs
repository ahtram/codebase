
using UnityEngine;
using UnityEngine.UI;

public class HeliosGamepadTester : MonoBehaviour {

    [SerializeField]
    private Helios _helios;

    //Ordered by Rhodos.InputType.
    public Image[] btnImages;

    public GameObject LJTracker;
    public GameObject RJTracker;
    public GameObject CJTracker;
    public GameObject CBJTracker;
    public GameObject VJTracker;

    public Slider LTSlider;
    public Slider RTSlider;

    private float TRACKER_RANGE = 40.0f;

    void Start() {
        //Listen to Helios.
        _helios.onGamepadLeftAxis.AddListener(OnLeftJoystick);
        _helios.onGamepadRightAxis.AddListener(OnRightJoystick);
        _helios.onGamepadCrossAxis.AddListener(OnCrossJoystick);
        _helios.onGamepadComboAxis.AddListener(OnComboJoystick);

        _helios.onVJoystickAxis.AddListener(OnVirtualJoystick);

        _helios.onInputDown.AddListener(OnControllerDown);
        _helios.onInputKeep.AddListener(OnControllerKeep);
        _helios.onInputUp.AddListener(OnControllerUp);

        _helios.onGamepadLTAxis.AddListener(OnControllerLTAxis);
        _helios.onGamepadRTAxis.AddListener(OnControllerRTAxis);
    }

    private void OnControllerDown(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length) {
            btnImages[(int)inputType].color = Color.gray;
        }
    }

    private void OnControllerKeep(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length) {
            btnImages[(int)inputType].color = Color.yellow;
        }
    }

    private void OnControllerUp(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length) {
            btnImages[(int)inputType].color = Color.white;
        }
    }

    private void OnLeftJoystick(Vector2 vec) {
        LJTracker.transform.localPosition = vec * TRACKER_RANGE;
    }

    private void OnRightJoystick(Vector2 vec) {
        RJTracker.transform.localPosition = vec * TRACKER_RANGE;
    }

    private void OnCrossJoystick(Vector2 vec) {
        CJTracker.transform.localPosition = vec * TRACKER_RANGE;
    }

    private void OnComboJoystick(Vector2 vec) {
        CBJTracker.transform.localPosition = vec * TRACKER_RANGE;
    }

    private void OnVirtualJoystick(Vector2 vec) {
        VJTracker.transform.localPosition = vec * TRACKER_RANGE;
    }

    private void OnControllerLTAxis(float value) {
        LTSlider.value = value;
    }

    private void OnControllerRTAxis(float value) {
        RTSlider.value = value;
    }
}
