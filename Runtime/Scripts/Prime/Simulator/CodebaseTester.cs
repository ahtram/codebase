using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodebaseTester : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        ScreenLogger.Log("This is a normal log.");
        ScreenLogger.LogGood("This is a good log.");
        ScreenLogger.LogWarning("This is a warning log.");
        ScreenLogger.LogError("This is an error log.");
        ScreenLogger.Log("This is a custom color log.", ColorPlus.Cyan);
    }

}
