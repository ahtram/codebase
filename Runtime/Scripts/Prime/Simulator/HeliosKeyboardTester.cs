
using UnityEngine;
using UnityEngine.UI;

public class HeliosKeyboardTester : MonoBehaviour {

    [SerializeField]
    private Helios _helios;

    //Ordered by Rhodos.InputType.
    public Image[] btnImages;

    public GameObject axisTracker;

    private float TRACKER_RANGE = 40.0f;

    void Start() {
        //Listen to Amphitrite.
        _helios.onInputDown.AddListener(OnKeyboardDown);
        _helios.onInputKeep.AddListener(OnKeyboardKeep);
        _helios.onInputUp.AddListener(OnKeyboardUp);
        _helios.onKeyboardAxis.AddListener(OnKeyboardAxis);
    }

    private void OnKeyboardDown(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length)
            btnImages[(int)inputType].color = Color.gray;
    }

    private void OnKeyboardKeep(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length)
            btnImages[(int)inputType].color = Color.yellow;
    }

    private void OnKeyboardUp(Rhodos.InputType inputType) {
        if ((int)inputType < btnImages.Length)
            btnImages[(int)inputType].color = Color.white;
    }

    private void OnKeyboardAxis(Vector2 vec) {
        axisTracker.transform.localPosition = vec * TRACKER_RANGE;
    }
}
