using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Teamuni.Codebase;
using Newtonsoft.Json;

public class MarkupHelperTester : MonoBehaviour {

    public TMP_InputField inputField;

    public void OnGoButton() {
        string outputStr = inputField.text;
        List<MarkupElementPlace> places = MarkupHelper.SeekAll(outputStr, "Tag");
        ///!!! We do it in invert order here.
        for (int i = places.Count - 1; i >= 0; --i) {
            outputStr = outputStr.Remove(places[i].openStartIndex, places[i].closeEndIndex - places[i].openStartIndex);
            outputStr = outputStr.Insert(places[i].openStartIndex, "[Inserted Text]");
        }
        Debug.Log(outputStr);
    }

}
