using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Teamuni.Codebase;
using Newtonsoft.Json;

public class WebLinkHelperTester : MonoBehaviour {

    public TMP_InputField inputField;

    public void OnGoButton() {
        string outputStr = inputField.text;
        List<WebLinkPlace> places = WebLinkHelper.SeekAllWebLinks(outputStr);
        ///!!! We do it in invert order here.
        //This is the example how we make a proper clickable link style.
        for (int i = places.Count - 1; i >= 0; --i) {
            outputStr = outputStr.Remove(places[i].startIndex, places[i].endIndex - places[i].startIndex);
            outputStr = outputStr.Insert(places[i].startIndex, Util.MarkupWithColorTag(Util.MarkupWithUnderline(WebLinkHelper.MarkupWithLinkTag(places[i].content, places[i].content)), Color.blue));
        }
        Debug.Log(outputStr);
    }

}
