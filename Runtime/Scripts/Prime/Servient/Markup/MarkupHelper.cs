using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teamuni.Codebase {

    static public class MarkupHelper {

        /// <summary>
        /// Seek the first markup element match the input tag.
        /// Return null if nothing found.
        /// Ex: <Tag>...</Tag>
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public MarkupElementPlace SeekFirst(string str, string tag) {
            if (!string.IsNullOrEmpty(tag) && !string.IsNullOrEmpty(str)) {
                string openTag = "<" + tag + ">";
                string closeTag = "</" + tag + ">";

                int openStartIndex = str.IndexOf(openTag);
                if (openStartIndex != -1) {
                    int openEndIndex = openStartIndex + openTag.Length;
                    //Found the opening index. Find the close index next.
                    int closeStartIndex = str.IndexOf(closeTag, openStartIndex + openTag.Length);
                    if (closeStartIndex != -1) {
                        int closeEndIndex = closeStartIndex + closeTag.Length;
                        string innerText = str.Substring(openStartIndex + openTag.Length, closeStartIndex - openStartIndex - openTag.Length);
                        return new MarkupElementPlace() {
                            openStartIndex = openStartIndex,
                            openEndIndex = openEndIndex,
                            closeStartIndex = closeStartIndex,
                            closeEndIndex = closeEndIndex,
                            innerText = innerText
                        };
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Seek all markup elements match the input tag.
        /// Ex: <Tag>...</Tag>
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public List<MarkupElementPlace> SeekAll(string str, string tag) {
            List<MarkupElementPlace> returnList = new List<MarkupElementPlace>();
            if (!string.IsNullOrEmpty(tag) && !string.IsNullOrEmpty(str)) {
                string openTag = "<" + tag + ">";
                string closeTag = "</" + tag + ">";
                int startIndex = 0;

                for (int i = 0; i < 100; ++i) {
                    int openStartIndex = str.IndexOf(openTag, startIndex);
                    if (openStartIndex != -1) {
                        int openEndIndex = openStartIndex + openTag.Length;
                        //Found the opening index. Find the close index next.
                        int closeStartIndex = str.IndexOf(closeTag, openStartIndex + openTag.Length);
                        if (closeStartIndex != -1) {
                            int closeEndIndex = closeStartIndex + closeTag.Length;
                            string innerText = str.Substring(openStartIndex + openTag.Length, closeStartIndex - openStartIndex - openTag.Length);
                            returnList.Add(new MarkupElementPlace() {
                                openStartIndex = openStartIndex,
                                openEndIndex = openEndIndex,
                                closeStartIndex = closeStartIndex,
                                closeEndIndex = closeEndIndex,
                                innerText = innerText
                            });
                            startIndex = closeStartIndex + closeTag.Length;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }

            }
            return returnList;
        }

    }

}
