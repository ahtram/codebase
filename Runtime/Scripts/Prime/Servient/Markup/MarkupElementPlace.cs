
using System.Xml.Serialization;

namespace Teamuni.Codebase {

    [XmlType("MEPL")]
    public class MarkupElementPlace {

        [XmlElement("OSI")]
        public int openStartIndex = 0;

        [XmlElement("OEI")]
        public int openEndIndex = 0;

        [XmlElement("CSI")]
        public int closeStartIndex = 0;

        [XmlElement("CEI")]
        public int closeEndIndex = 0;

        [XmlElement("IT")]
        public string innerText = "";

    }

}

