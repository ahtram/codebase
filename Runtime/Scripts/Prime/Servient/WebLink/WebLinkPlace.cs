using System.Xml.Serialization;

namespace Teamuni.Codebase {

    [XmlType("WLPL")]
    public class WebLinkPlace {

        [XmlElement("SI")]
        public int startIndex = 0;

        [XmlElement("EI")]
        public int endIndex = 0;

        [XmlElement("CT")]
        public string content = "";

    }

}

