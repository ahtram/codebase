using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Teamuni.Codebase {
    static public class WebLinkHelper {

        /// <summary>
        /// Seek the first web link.
        /// Return null if nothing found.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public WebLinkPlace SeekFirstWebLink(string str) {
            if (!string.IsNullOrEmpty(str)) {
                return SeekFirstProtocal(str, new List<string>() { "http", "https" });
            }
            return null;
        }

        /// <summary>
        /// Seek all first web links.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public List<WebLinkPlace> SeekAllWebLinks(string str) {
            if (!string.IsNullOrEmpty(str)) {
                return SeekAllProtocal(str, new List<string>() { "http", "https" });
            }
            return new List<WebLinkPlace>();
        }

        /// <summary>
        /// Seek the first Protocal link.
        /// Return null if nothing found.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public WebLinkPlace SeekFirstProtocal(string str, List<string> protocals) {
            for (int o = 0; o < protocals.Count; ++o) {
                if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(protocals[o])) {
                    string prefixProtocal = protocals[o] + "://";
                    char[] endChars = new char[] {
                        '\n',
                        ' ',
                    };

                    int openStartIndex = str.IndexOf(prefixProtocal);
                    if (openStartIndex != -1) {
                        //Found the open index. Find the close index next.
                        int closeStartIndex = str.IndexOfAny(endChars, openStartIndex + prefixProtocal.Length);
                        if (closeStartIndex != -1) {
                            return new WebLinkPlace() {
                                startIndex = openStartIndex,
                                endIndex = closeStartIndex,
                                content = str.Substring(openStartIndex, closeStartIndex - openStartIndex),
                            };
                        } else {
                            //This means the link is at end of the string.
                            return new WebLinkPlace() {
                                startIndex = openStartIndex,
                                endIndex = str.Length,
                                content = str.Substring(openStartIndex, str.Length - openStartIndex),
                            };
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Seek all Protocal links.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public List<WebLinkPlace> SeekAllProtocal(string str, List<string> protocals) {
            List<WebLinkPlace> returnList = new List<WebLinkPlace>();
            for (int o = 0; o < protocals.Count; ++o) {
                if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(protocals[o])) {
                    string prefixProtocal = protocals[o] + "://";
                    char[] endChars = new char[] {
                        '\n',
                        ' ',
                    };
                    int startIndex = 0;

                    for (int i = 0; i < 100; ++i) {
                        int openStartIndex = str.IndexOf(prefixProtocal, startIndex);
                        if (openStartIndex != -1) {
                            //Found the open index. Find the close index next.
                            int closeStartIndex = str.IndexOfAny(endChars, openStartIndex + prefixProtocal.Length);
                            if (closeStartIndex != -1) {
                                returnList.Add(new WebLinkPlace() {
                                    startIndex = openStartIndex,
                                    endIndex = closeStartIndex,
                                    content = str.Substring(openStartIndex, closeStartIndex - openStartIndex),
                                });
                                startIndex = closeStartIndex;
                                continue;
                            } else {
                                //This means the link is at end of the string.
                                returnList.Add(new WebLinkPlace() {
                                    startIndex = openStartIndex,
                                    endIndex = str.Length,
                                    content = str.Substring(openStartIndex, str.Length - openStartIndex),
                                });
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }

            //Sort the returnList by startIndex!
            return returnList.OrderBy(p => p.startIndex).ToList();
        }

        static public string MarkupWithLinkTag(string str, string ID) {
            return ("<link=\"" + ID + "\">" + str + "</link>");
        }

    }

}
