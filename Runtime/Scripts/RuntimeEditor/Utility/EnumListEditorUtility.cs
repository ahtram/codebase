#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// A generic utility editor for conveniently edit any Enum list.
/// </summary>
abstract public class EnumListEditorUtility<T> : UniEditorWindow where T : System.Enum {

    protected Vector2 m_scrollPos = Vector2.zero;

    protected List<T> editingEnumList = null;
    protected string listTitle = "";
    protected Action<List<T>> onClose;

    public override void OnGUI() {
        base.OnGUI();
        if (editingEnumList != null) {
            EditorGUILayout.BeginVertical("FrameBox");
            {
                m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
                {
                    EditorGUILayoutPlus.EditEnumList(listTitle, editingEnumList);
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }
    }

    private void OnDestroy() {
        if (onClose != null) {
            onClose(editingEnumList);
        }
    }

}

#endif