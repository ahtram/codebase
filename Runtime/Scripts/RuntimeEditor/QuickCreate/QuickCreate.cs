#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using System.Linq;

//Quick create things into the current scene. (From selection assets)
public class QuickCreate {

    [MenuItem("Assets/Create In Scene/Sprite Renderer", false, 0)]
    static public void QuickCreateSpriteRenderers() {
        //Try create GameObjects with SpriteRenderer into the scene root.
        foreach (Object obj in Selection.objects) {
            if (obj is Sprite) {
                GameObject newGO = new GameObject(obj.name, typeof(SpriteRenderer));
                newGO.GetComponent<SpriteRenderer>().sprite = obj as Sprite;
            }
        }
    }

    [MenuItem("Assets/Create In Scene/Sprite Renderer", true, 0)]
    static public bool QuickCreateSpriteRenderersValidate() {
        //Check if any selection object is a Sprite asset.
        return Selection.objects.Any(x => x is Sprite);
    }

}

#endif
